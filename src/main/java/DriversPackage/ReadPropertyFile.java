package DriversPackage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ReadPropertyFile {

    private Properties prop;

    public ReadPropertyFile(){
        try {
            FileInputStream input = new FileInputStream("src\\main\\resources\\config.properties");
            prop = new Properties();
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String readProperty(String property){
        return prop.getProperty(property);
    }


}
