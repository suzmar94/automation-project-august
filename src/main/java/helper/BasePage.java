package helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 5);
    }
    public void clickElement(By locator){
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
        driver.findElement(locator).click();
    }
    public void setElementsText(By locator,String text){
        try{
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
            driver.findElement(locator).sendKeys(text);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    public String getElementsText(By locator){
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
        return driver.findElement(locator).getText();
    }
}
