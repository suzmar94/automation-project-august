package page;

import helper.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class AddToCartBtnPage extends BasePage {

    public AddToCartBtnPage(WebDriver driver) {
        super(driver);
    }

    protected By title = By.className("title");
    protected By addToCartBtn = By.id("add-to-cart-sauce-labs-backpack");
    protected By cartIcon = By.className("shopping_cart_link");

    public void clickOnAddToCartBtn(){
        clickElement(addToCartBtn);
    }
    public void clickOnCart(){
        clickElement(cartIcon);
    }
    public void isPageLoaded(){
        Assert.assertEquals(getElementsText(title),"PRODUCTS");
    }
}
