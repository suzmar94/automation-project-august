package page;

import helper.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage extends BasePage {
    protected By checkoutBtn = By.id("checkout");
    public CartPage(WebDriver driver) {
        super(driver);
    }
    public void clickOnCheckOutBtn(){
        clickElement(checkoutBtn);
    }
}
