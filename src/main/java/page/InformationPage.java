package page;

import helper.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InformationPage extends BasePage {
    protected By firstName = By.id("first-name");
    protected By lastName = By.id("last-name");
    protected By zipCode = By.id("postal-code");
    protected By continueBtn = By.id("continue");

    public InformationPage(WebDriver driver) {
        super(driver);
    }
    public void enterFirstName(String name){
        setElementsText(firstName, name);
    }
    public void enterLastName(String username){
        setElementsText(lastName, username);
    }
    public void enterZipCode(String username){
        setElementsText(zipCode, username);
    }
    public void clickOnContinueBtn(){
        clickElement(continueBtn);
    }
}
