package page;

import helper.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class LoginPage extends BasePage {
    protected By usernameField = By.id("user-name");
    protected By passwordField = By.id("password");
    protected By loginBtn = By.id("login-button");
    protected By errorMessage = By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]/h3");

    public LoginPage(WebDriver driver) {
        super(driver);
    }
    public void setUsername(String text){
        setElementsText(usernameField, text);
    }
    public void setPassword(String password){
        setElementsText(passwordField, password);
    }
    public void clickLoginBtn(){
        clickElement(loginBtn);
    }
    public void doesErrorAppears(){
        Assert.assertEquals(getElementsText(errorMessage),"Epic sadface: Username and password do not match any user in this service");
    }

}
