package page;

import helper.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FinishTheOrderPage extends BasePage {
    protected By finishBtn = By.id("finish");
    public FinishTheOrderPage(WebDriver driver) {
        super(driver);
    }
    public void clickOnFinishBtn(){
        clickElement(finishBtn);
    }
}
