package page;

import helper.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BackToHomePage extends BasePage {
    protected By backToHomeBtn = By.id("back-to-products");
    public BackToHomePage(WebDriver driver) {
        super(driver);
    }
    public void clickOnBackToHomeBtn(){
        clickElement(backToHomeBtn);
    }
}
