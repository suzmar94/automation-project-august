package tests;

import DriversPackage.DriverManager;
import DriversPackage.ReadPropertyFile;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public class BaseTest
{
    protected WebDriver driver;
    protected ReadPropertyFile readPropFile = new ReadPropertyFile();

    @BeforeMethod
    public void testInit()
    {
        driver = DriverManager.getDriver();
        driver.manage().window().maximize();
        driver.get(readPropFile.readProperty("url"));
    }

    @AfterMethod
    public void testTearDown() {
        driver.quit();
    }

}