package tests;

import org.testng.annotations.Test;
import page.*;

public class CheckOutTest extends BaseTest{

    @Test
    public void firstTest() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.setUsername("standard_user");
        loginPage.setPassword("secret_sauce");
        loginPage.clickLoginBtn();

        AddToCartBtnPage addToCrt = new AddToCartBtnPage(driver);
        addToCrt.clickOnAddToCartBtn();
        addToCrt.clickOnCart();

        CartPage checkout = new CartPage(driver);
        checkout.clickOnCheckOutBtn();

        InformationPage fillInTheInformation = new InformationPage(driver);
        fillInTheInformation.enterFirstName("Suzana");
        fillInTheInformation.enterLastName("Markovic");
        fillInTheInformation.enterZipCode("11030");
        fillInTheInformation.clickOnContinueBtn();

        FinishTheOrderPage finish = new FinishTheOrderPage(driver);
        finish.clickOnFinishBtn();

        BackToHomePage backToHome = new BackToHomePage(driver);
        backToHome.clickOnBackToHomeBtn();

        addToCrt.isPageLoaded();




        //due to visual confirmation
        Thread.sleep(4000);
    }
}
