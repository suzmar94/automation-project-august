package tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.AddToCartBtnPage;
import page.LoginPage;

public class LogInFormTest extends BaseTest{
    @Test
    public void successfulLogin(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.setUsername("standard_user");
        loginPage.setPassword("secret_sauce");
        loginPage.clickLoginBtn();
        AddToCartBtnPage addToCart = new AddToCartBtnPage(driver);
        addToCart.isPageLoaded();
    }

    @Test(dataProvider = "wrongLoginCredentials")
    public void unsuccessfulLogin(String username, String password){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.setUsername(username);
        loginPage.setPassword(password);
        loginPage.clickLoginBtn();
        loginPage.doesErrorAppears();
    }

    @DataProvider(name = "wrongLoginCredentials")
    public static Object[][] wrongLoginCredentials(){
        return new Object[][]{
                {"standard_user", "wrongPassword"},
                {"wrongUser", "secret_sauce"},
                {"wrongUsername", "wrongPassword"}
        };
    }

}
